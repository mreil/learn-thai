# Learn Thai #

This is a collection of resources that I find useful when learning Thai.


## General ##

* [thai-language.com](http://thai-language.com/): dictionary, lessons, quizzes, etc.


## YouTube ##

* [Learn Thai with Mod](https://www.youtube.com/channel/UCxf3zYDZw9NjUllgsCGyBmg)


## Reading ##

* [Fables Thai-English](http://fables-th-en.blogspot.com/)
* [Learn Thai with Manee](http://ressources.learn2speakthai.net/): Reading books for primary 
	school published by the Thai Ministry of Education a long time ago, 
	Google Drive, Sign-up required
* Daisy Stories, short stories with English translation and phonetics (kind of)
	* http://www.lonweb.org/daisy/ds-thai-nightwatch.htm
	* http://www.lonweb.org/daisy/ds-thai-littletrip.htm
	* http://www.lonweb.org/daisy/ds-thai-bookworm.htm
	* http://www.lonweb.org/daisy/ds-thai-macbeth.htm
